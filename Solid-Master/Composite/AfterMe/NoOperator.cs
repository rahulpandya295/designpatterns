﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    class NoOperator : BaseOperator
    {
        private string _value;
        public NoOperator(string value)
        {
            _value = value;
        }
        protected override OperatorType OperateType { get { return OperatorType.None; } }

        public override void Operate()
        {
            Console.WriteLine(_value);
        }
    }
}
