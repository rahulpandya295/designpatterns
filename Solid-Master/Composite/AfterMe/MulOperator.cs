﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    class MulOperator : BaseOperator
    {
        protected override OperatorType OperateType { get { return OperatorType.Mul; } }

        public override void Operate()
        {
            Console.WriteLine("mul");
        }
    }
}
