﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    abstract class BaseOperator : IOperator
    {
        protected abstract OperatorType OperateType {get;}

        public abstract void Operate();
    }
}
