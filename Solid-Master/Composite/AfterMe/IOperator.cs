﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    interface IOperator
    {
        void Operate();
    }
}
