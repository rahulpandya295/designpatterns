﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    class AddOperator : BaseOperator
    {
        protected override OperatorType OperateType { get { return OperatorType.Add; } }

        public override void Operate()
        {
            Console.WriteLine("add");
        }
    }
}
