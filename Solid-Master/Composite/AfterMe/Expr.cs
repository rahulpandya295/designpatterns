﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    internal class Expr
    {
        private readonly Expr _left;
        private readonly Expr _right;
        private readonly IOperator _value;

        public Expr(Expr left, IOperator value, Expr right)
        {
            _left = left;
            _value = value;
            _right = right;
        }

        public void GenCode()
        {
            if (_left == null && _right == null)
            {
                _value.Operate();
            }
            else
            {
                // its an intermediate node 
                _left.GenCode();
                _right.GenCode();
                _value.Operate();
            }
        }
    }

    internal class ExprMain
    {
        public static void Main()
        {
            // ((10 * 20) + 30)  
            var expr = new Expr(
                new Expr(
                    new Expr(null, new NoOperator("10"), null), 
                    new MulOperator(), 
                    new Expr(null, new NoOperator("20"), null)),
                new AddOperator(),
                new Expr(null, new NoOperator("30"), null));
            expr.GenCode();
            Console.ReadLine();
        }
    }
}
