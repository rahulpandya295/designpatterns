﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Composite.AfterMe
{
    enum OperatorType
    {
        None = 0,
        Add = 1,
        Mul = 2
    }
}
