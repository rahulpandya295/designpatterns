﻿// This code uses constructor for the node objects in the expression tree
// Instead, refactor this code to introduce factory methods 

using System;
using System.Collections.Generic;

namespace Solid_Master.FactoryMethod.AfterMe
{
    internal abstract class Expr
    {
        public abstract void GenCode();
    }

    internal class Constant : Expr
    {
        private readonly int _val;

        private static Dictionary<int, Constant> pool = new Dictionary<int, Constant>();

        public Constant(int val)
        {
            _val = val;
        }

        public override void GenCode()
        {
            Console.WriteLine("ldc.i4.s " + _val);
        }

        public static Expr Make(int val)
        {
            if (!pool.ContainsKey(val))
            {
                pool[val] = new Constant(val);
            }

            return pool[val];
        }
    }

    internal class BinaryExpr : Expr
    {
        private readonly Expr _left;
        private readonly Expr _right;

        public BinaryExpr(Expr arg1, Expr arg2)
        {
            _left = arg1;
            _right = arg2;
        }

        public override void GenCode()
        {
            _left.GenCode();
            _right.GenCode();
        }
    }

    internal class Plus : BinaryExpr
    {
        public Plus(Expr arg1, Expr arg2) : base(arg1, arg2)
        {
        }

        public override void GenCode()
        {
            base.GenCode();
            Console.WriteLine("add");
        }

        public static Plus Make(Expr left, Expr right)
        {
            return new Plus(left, right);
        }
    }

    internal class Multiply : BinaryExpr
    {
        public Multiply(Expr arg1, Expr arg2) : base(arg1, arg2)
        {
        }

        public override void GenCode()
        {
            base.GenCode();
            Console.WriteLine("mul");
        }

        public static Multiply Make(Expr left, Expr right)
        {
            return new Multiply(left, right);
        }
    }    

    internal class FactoryMethodMain
    {
        public static void Main()
        {
            // ((10 * 20) + 10)  
            Expr expr = Plus.Make(Multiply.Make(Constant.Make(10), Constant.Make(20)), Constant.Make(30));
            expr.GenCode();            
        }
    }
}