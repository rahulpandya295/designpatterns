﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Solid_Master.Null.After
{
    class SoldArticleTests
    {
        public static void Main(string[] args)
        {
            SoldArticle sold = null;
            bool response = false;

            sold = new SoldArticle(DateTime.Now, new DuratedWarranty(TimeSpan.FromMinutes(3)));
            response = sold.Claim(DateTime.Now);
            Assert.IsTrue(response);

            sold = new SoldArticle(DateTime.Now, new DuratedWarranty(TimeSpan.FromMinutes(3)));
            sold.VisibleDamageDone();
            response = sold.Claim(DateTime.Now);
            Assert.IsFalse(response);

            sold = new SoldArticle(DateTime.Now, new DuratedWarranty(TimeSpan.FromMinutes(3)));
            sold.VisibleDamageDone();
            response = sold.Claim(DateTime.Now);
            Assert.IsFalse(response);
        }
    }
}
