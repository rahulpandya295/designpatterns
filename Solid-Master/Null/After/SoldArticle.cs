﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Null.After
{
    public class SoldArticle
    {
        private readonly DateTime _purchaseDate;
        private IWarranty warranty;
        public SoldArticle(DateTime purchasedDate, IWarranty warranty)
        {
            this._purchaseDate = purchasedDate;
            this.warranty = warranty;
        }

        public void VisibleDamageDone()
        {
            this.warranty = warranty.Void();
        }

        public bool Claim(DateTime claimDate)
        {
            return this.warranty.Claim(_purchaseDate, claimDate);
        }
    }
}
