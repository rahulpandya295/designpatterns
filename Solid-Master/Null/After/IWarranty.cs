﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Null.After
{
    public interface IWarranty
    {
        bool Claim(DateTime _purchaseDate, DateTime claimDate);

        IWarranty Void();
    }
}
