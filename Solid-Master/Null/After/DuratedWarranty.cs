﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Null.After
{
    public class DuratedWarranty : IWarranty
    {
        public TimeSpan _duration;

        public DuratedWarranty(TimeSpan _duration)
        {
            this._duration = _duration;
        }

        public bool Claim(DateTime _purchaseDate, DateTime claimDate)
        {
            return _purchaseDate + _duration >= claimDate;
        }

        public IWarranty Void()
        {
            return new VoidWarranty();
        }
    }
}
