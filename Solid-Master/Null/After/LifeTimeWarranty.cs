﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.Null.After
{
    class LifeTimeWarranty : IWarranty
    {
        public bool Claim(DateTime _purchaseDate, DateTime claimDate)
        {
            return true;
        }

        public IWarranty Void()
        {
            return new VoidWarranty();
        }
    }
}
