﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Solid_Master.TemplateMethod.Before
{
    class JobProcessor
    {
        public static void Main(string[] args)
        {
            //Read all CSV files in the directory
            foreach (var filePath in Directory.GetFiles(args[1]))
            {
                int lineNumber = 0;
                List<string> errors = new List<string>();

                //For each line in the file
                foreach (var line in File.ReadLines(filePath))
                {
                    lineNumber++;
                    //Expect NAME, DOB, EMPLOYEEID
                    var values = line.Split(',');

                    //Get values
                    string name = values[0];
                    string dob = values[1];
                    string employeeid = values[2];
                    
                    //Validate
                    if(string.IsNullOrEmpty(name))
                        errors.Add($"[Line {lineNumber}]: Name is empty or null");
                    if (string.IsNullOrEmpty(dob))
                        errors.Add($"[Line {lineNumber}]: DOB is empty or null");
                    if(!DateTime.TryParse(dob, out DateTime date))
                        errors.Add($"[Line {lineNumber}]: DOB is invalid");
                    if(!int.TryParse(employeeid, out int id))
                        errors.Add($"[Line {lineNumber}]: DOB is invalid");
                    if(string.IsNullOrEmpty(employeeid))
                        errors.Add($"[Line {lineNumber}]: DOB is empty or null");

                    //Load to database
                    LoadToDatabase(new Employee {DOB = date, EmployeeId = id, Name = name});
                }
            }
        }

        public static void LoadToDatabase(Employee employee)
        {
            //Database error handling
        }
    }

    internal class Employee
    {
        public  string Name { get; set; }
        public int EmployeeId { get; set; }
        public DateTime DOB { get; set; }
    }
}
