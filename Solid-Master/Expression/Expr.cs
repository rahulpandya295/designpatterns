﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solid_Master.Composite.AfterMe;

namespace Solid_Master.Expression
{
    class Expr2
    {
        static void Main(string[] args)
        {
            ////int a = 1, b = 2, c = 3, d = 4, e = 5, f = 6;
            ////int r = (a - (b / c)) + ((d % e) * f);
            ////Console.WriteLine(r);

            // ((10 * 20) + 30)  
            var expr = new Expr(
                new Expr(
                    new Expr(null, new NoOperator("10"), null),
                    new MulOperator(),
                    new Expr(null, new NoOperator("20"), null)),
                new AddOperator(),
                new Expr(null, new NoOperator("30"), null));
            expr.GenCode();
            Console.ReadLine();
        }
    }
}
