﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    public interface IState
    {
        IState GetInstance();
        IState OnDeposit(int val, Action AddBalance);
        IState OnWithdraw(int val, Action RemoveBalance);
        IState OnClose();
        IState OnFreeze();
        IState OnVerify();
    }
}
