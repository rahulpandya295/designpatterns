﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    public class ActiveState : BaseState
    {
        public override StateType stateType { get { return StateType.Active; } }

        public override IState OnClose()
        {
            return new ClosedState();
        }

        public override IState OnDeposit(int val, Action AddBalance)
        {
            AddBalance();
            return this;
        }

        public override IState OnFreeze()
        {
            throw new NotImplementedException();
        }

        public override IState OnVerify()
        {
            throw new NotImplementedException();
        }

        public override IState OnWithdraw(int val, Action RemoveBalance)
        {
            RemoveBalance();
            return this;
        }
    }
}
