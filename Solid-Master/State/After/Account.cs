﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    class Account
    {
        public int balance { get; private set; }
        public IState currentState;

        public Account()
        {
            balance = 0;
            currentState = new ActiveState();
        }

        public void AddBalance(int val)
        {
            balance += val;
        }

        public void RemoveBalance(int val)
        {
            balance -= val;
        }

        public void Deposit(int val)
        {
            currentState = currentState.OnDeposit(val, () => balance += val);            
        }
    }
}
