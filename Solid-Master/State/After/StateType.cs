﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    public enum StateType
    {
        NotVerified = 0,
        Active = 1,
        Frozen = 2,
        Closed = 3
    }
}
