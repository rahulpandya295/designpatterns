﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    public abstract class BaseState : IState
    {
        public abstract StateType stateType { get; }

        public IState GetInstance()
        {
            return this;
        }

        public abstract IState OnClose();

        public abstract IState OnDeposit(int val, Action AddBalance);

        public abstract IState OnFreeze();

        public abstract IState OnVerify();

        public abstract IState OnWithdraw(int val, Action RemoveBalance);
    }
}
