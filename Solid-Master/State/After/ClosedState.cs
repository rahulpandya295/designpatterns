﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid_Master.State.After
{
    public class ClosedState : BaseState
    {
        public override StateType stateType { get { return StateType.Closed; } }

        public override IState OnClose()
        {
            return this;
        }

        public override IState OnDeposit(int val, Action AddBalance)
        {
            return this;
        }

        public override IState OnFreeze()
        {
            return this;
        }

        public override IState OnVerify()
        {
            return this;
        }

        public override IState OnWithdraw(int val, Action RemoveBalance)
        {
            return this;
        }
    }
}
