﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Solid_Master.State.After
{
    class AccountTests
    {
        public static void Main(string[] args)
        {
            var acc = new Account();
            Console.WriteLine(acc.balance);
            acc.Deposit(10);
            Console.WriteLine(acc.balance);
        }
    }
}
