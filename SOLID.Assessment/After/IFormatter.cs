﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public interface IFormatter
    {
        string PrintStatus(List<IDevice> devices);
    }
}
