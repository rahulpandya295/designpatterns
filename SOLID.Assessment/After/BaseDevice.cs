﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public abstract class BaseDevice : IDevice
    {
        protected abstract DeviceType DeviceType { get; }

        protected abstract DeviceProperty Property { get; set; }

        protected bool Status { get; set; }
        
        protected string DeviceId { get; set; }

        public BaseDevice(string DeviceId)
        {
            this.Status = true;
            this.DeviceId = DeviceId;
        }

        public string GetStatus()
        {
            string onOrOff = this.Status ? DeviceResources.On : DeviceResources.Off;

            return $"{DeviceResources.Device}: {onOrOff},  {this.Property.Value}";
        }

        public void ToggleStatus()
        {
            Status = !Status;
        }

        public DeviceType GetDeviceType()
        {
            return this.DeviceType;
        }

        public string GetDeviceId()
        {
            return this.DeviceId;
        }
    }
}
