﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public class HtmlFormatter : IFormatter
    {
        public string PrintStatus(List<IDevice> devices)
        {
            var returnString = string.Empty;

            // test list is empty
            if (devices.Count == 0)
            {
                returnString = $"<h1>{DeviceResources.EmptyList}!</h1>";
            }
            else
            {
                //we have devices
                //header
                returnString += $"<h1>{DeviceResources.DeviceReport}</h1><br/>";                

                foreach (var device in devices)
                {
                    returnString += $"<BR/> Device ID: {device.GetDeviceId()};{device.GetStatus()}<BR/>";
                }

                int totalCount = 0;

                var deviceTypes = devices.Select(d => d.GetDeviceType()).Distinct();

                foreach (var dt in deviceTypes)
                {
                    int count = 0;
                    count = GetCount(devices, dt);
                    returnString += GetLine(dt, count);
                    totalCount += count;
                }

                //footer
                returnString += "TOTAL: ";
                returnString += totalCount + " " + DeviceResources.Devices;
            }
            return returnString;
        }

        private static int GetCount(List<IDevice> devices, DeviceType deviceType)
        {
            return devices.Count(d => d.GetDeviceType() == deviceType);
        }

        private static string GetLine(DeviceType type, int num)
        {
            if (num > 0)
            {
                return $"{num} {DeviceTypeMethods.Translate(type)} {DeviceResources.Devices}<br/>";
            }
            return string.Empty;
        }
    }
}
