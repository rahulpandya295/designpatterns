﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public class DeviceProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
