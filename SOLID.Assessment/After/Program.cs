﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IFormatter format = new HtmlFormatter();
            format.PrintStatus(new List<IDevice>()
            {
                new MedicalDevice(new DeviceProperty(){ Value = "75" }, "medical1"),
                new AgriculturalDevice(new DeviceProperty(){ Value = "100" }, "agri1")
            });
        }
    }
}
