﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public class AgriculturalDevice : BaseDevice
    {
        public AgriculturalDevice(DeviceProperty property, string DeviceId) : base(DeviceId)
        {
            Property = property;
        }

        protected override DeviceType DeviceType { get { return DeviceType.AGRICULTURAL; } }

        protected override DeviceProperty Property { get; set; }
    }
}
