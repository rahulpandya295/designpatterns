﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public enum DeviceType
    {
        MEDICAL = 0,
        AGRICULTURAL = 1,
        REFINARY = 2
    }

    public static class DeviceTypeMethods
    {
        public static string Translate(DeviceType type)
        {
            switch (type)
            {
                case DeviceType.MEDICAL:
                    return DeviceResources.Medical;
                case DeviceType.AGRICULTURAL:
                    return DeviceResources.Agricultural;
                case DeviceType.REFINARY:
                    return DeviceResources.Refinary;
            }
            return string.Empty;
        }
    }
}
