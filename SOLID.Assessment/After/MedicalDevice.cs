﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID.Assessment.After
{
    public class MedicalDevice : BaseDevice
    {
        protected override DeviceType DeviceType { get { return DeviceType.MEDICAL; }}

        protected override DeviceProperty Property { get; set; }

        public MedicalDevice(DeviceProperty property, string DeviceId) : base (DeviceId)
        {
            Property = property;
        }
        
    }
}
