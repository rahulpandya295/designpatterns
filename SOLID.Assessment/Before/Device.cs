﻿using System.Collections.Generic;

namespace SOLID.Assessment.Before
{
    public enum DeviceType
    {
        MEDICAL = 0,
        AGRICULTURAL = 1,
        REFINARY = 2
    }

    public class Device
    {
        public int DeviceId;
        public int HeartRate;
        public int SoilQuality;
        public bool Status = true;
        public int Temperature;
        public DeviceType Type;

        public Device(DeviceType type, int deviceId)
        {
            Type = type;
            DeviceId = deviceId;
        }

        public static string GetStatus(Device device)
        {
            string onOrOff = device.Status ? DeviceResources.On : DeviceResources.Off;

            switch (device.Type)
            {
                case DeviceType.MEDICAL:
                    return $"{DeviceResources.Device}: {onOrOff},  {device.HeartRate}";
                case DeviceType.AGRICULTURAL:
                    return $"{DeviceResources.Device}: {onOrOff},  {device.SoilQuality}";
                case DeviceType.REFINARY:
                    return $"{DeviceResources.Device}: {onOrOff},  {device.Temperature}";
            }

            return string.Empty;
        }

        public static string PrintStatus(List<Device> devices)
        {
            var returnString = string.Empty;

            // test list is empty
            if (devices.Count == 0)
            {
                returnString = $"<h1>{DeviceResources.EmptyList}!</h1>";
            }
            else
            {
                //we have devices
                //header
                returnString += $"<h1>{DeviceResources.DeviceReport}</h1><br/>";

                var numberMedical = 0;
                var numberAgricultural = 0;
                var numberRefinaries = 0;

                //Get all statuses
                foreach (var device in devices)
                {
                    if (device.Type == DeviceType.MEDICAL)
                        numberMedical++;
                   
                    if (device.Type == DeviceType.AGRICULTURAL)
                        numberAgricultural++;
                    
                    if (device.Type == DeviceType.REFINARY)
                        numberRefinaries++;
                    
                    returnString += $"<BR/> Device ID: {device.DeviceId};{GetStatus(device)}<BR/>";
                }

                //let`s print this
                returnString += GetLine(DeviceType.MEDICAL, numberMedical);
                returnString += GetLine(DeviceType.AGRICULTURAL, numberAgricultural);
                returnString += GetLine(DeviceType.REFINARY, numberRefinaries);

                //footer
                returnString += "TOTAL: ";
                returnString += numberAgricultural + numberMedical + numberRefinaries + " " + DeviceResources.Devices;
            }
            return returnString;
        }


        public static string GetLine(DeviceType type, int num)
        {
            if (num > 0)
            {
                return $"{num} {Translate(type)} {DeviceResources.Devices}<br/>";
            }
            return string.Empty;
        }

        private static string Translate(DeviceType type)
        {
            switch (type)
            {
                case DeviceType.MEDICAL:
                    return DeviceResources.Medical;
                case DeviceType.AGRICULTURAL:
                    return DeviceResources.Agricultural;
                case DeviceType.REFINARY:
                    return DeviceResources.Refinary;
            }
            return string.Empty;
        }

        public void ToggleStatus()
        {
            Status = !Status;
        }
    }
}