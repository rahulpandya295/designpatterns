﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace SOLID.Assessment.Before
{
    [TestFixture]
    public class DeviceTests
    {
        [Test]
        public void PrintDeviceStatusInEnglish()

        {
            List<Device> devices = new List<Device>
            {
                new Device(DeviceType.MEDICAL, 1) {Status = false, HeartRate = 75},
                new Device(DeviceType.AGRICULTURAL, 2) {Status = true, SoilQuality = 32},
                new Device(DeviceType.REFINARY, 3) {Status = false, Temperature = 60}
            };

            var printStatus = Device.PrintStatus(devices);

            var expectedString =
                "<h1>Devices report</h1><br/><BR/> Device ID: 1;Device: Off,  75<BR/><BR/> Device ID: 2;Device: On,  32<BR/><BR/> Device ID: 3;Device: Off,  60<BR/>1 Medical Devices<br/>1 Agricultural Devices<br/>1 Refinary Devices<br/>TOTAL: 3 Devices";

            Assert.AreEqual(expectedString, printStatus);
        }

        [Test]
        public void PrintDeviceStatusInDutch()

        {
            List<Device> devices = new List<Device>
            {
                new Device(DeviceType.MEDICAL, 1) {Status = false, HeartRate = 75},
                new Device(DeviceType.AGRICULTURAL, 2) {Status = true, SoilQuality = 32},
                new Device(DeviceType.REFINARY, 3) {Status = false, Temperature = 60}
            };
            
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("nl-NL");
            var printStatus = Device.PrintStatus(devices);
            
            var expectedString =
                "<h1>Apparaten Rapport</h1><br/><BR/> Device ID: 1;Apparaat: Uit,  75<BR/><BR/> Device ID: 2;Apparaat: Op,  32<BR/><BR/> Device ID: 3;Apparaat: Uit,  60<BR/>1 Medisch Apparaten<br/>1 Agrarisch Apparaten<br/>1 Raffinaderij Apparaten<br/>TOTAL: 3 Apparaten";

            Assert.AreEqual(expectedString, printStatus);
        }

        [Test]
        public void ToggleStatus()
        {
            var device = new Device(DeviceType.MEDICAL, 1) { Status = false, HeartRate = 75 };
            device.ToggleStatus();

            Assert.AreEqual(device.Status, true);
            device.ToggleStatus();

            Assert.AreEqual(device.Status, false);

        }

        [Test]
        public void PrintEmptyDeviceStatusInEnglish()

        {
            List<Device> devices = new List<Device>();

            var printStatus = Device.PrintStatus(devices);

            var expectedString = "<h1>Empty list of devices!</h1>";

            Assert.AreEqual(expectedString, printStatus);
        }


        [Test]
        public void PrintEmptyDeviceStatusInDutch()
        {
            List<Device> devices = new List<Device>();

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("nl-NL");

            var printStatus = Device.PrintStatus(devices);
            
            var expectedString = "<h1>Lege lijst met apparaten!</h1>";

            Assert.AreEqual(expectedString, printStatus);
        }
    }
}
